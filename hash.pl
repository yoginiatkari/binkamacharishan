#!/usr/bin/perl


@exper = ("perl","python","java","c");
@exper = sort(@exper);
print "@exper\n";

%skill = (unix => 5, Perl => 4, Python => 2, Java => 1);

@skill = keys %skill;
print "keys = @skill\n";

@skillValue = values %skill;
print "values = @skillValue\n";

if(exists($skill{"LDAP"})){
	print "Exists!!\n";
}else{
	print "Not Exist!!\n";
}
if(!exists($skill{"Perl"})){
	print "Not Exists!!\n";
}else{
	print "Exist!!\n";
}

print scalar @skill, "\n";
print scalar @skillValue, "\n";

$skill{"Oracle"} = 7;
print $skill{"Oracle"}, "\n";

delete $skill{"Oracle"};
print $skill{"Oracle"}, "\n";
